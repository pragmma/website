const team = [
    {
        name: "Josh Vazquez",
        id: "josh-vazquez",
        position: "Full Stack Developer & Founder",
        description: [
            'With a vision on enhancing the world through technology, his focus is filling the gap between ideas and people able to execute them'
            ,
            'He is a practicing developer, a public speaker, and  Director of Pragmma.',
            'With more than 7 years in software development, Josh has worked in a variety of roles focused on creating rich and interactive experiences.'
            ,
            'Outside of coding websites, Josh is a proud geek with an interest in technology, science fiction and board games. He is also a likes sports and does yoga everyday.'
        ],
        social: [
            { platform: 'linkedin', username: 'joshVazq' },
            { platform: 'web', username: 'http://joshvazq.gitlab.io' },
            /*  { platform: 'instagram', username: 'joshVazq' },
             { platform: 'twitter', username: 'joshVazq' },
             { platform: 'facebook', username: 'joshVazq' } */
        ]
    },

    {
        name: "Noah Bennet",
        id: "noah-bennet",

        position: "Office Kitten & Founder",
        description: [
            'Noah loves resting on top of the laptop. His job is to meet and greet visitors, help anyone looking for a quick break away of the laptop, and keep the kitchen floor clean.'
            , 'A great card games player with an amazing poker face.',

        ]
    }

];





export default team;