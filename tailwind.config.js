const defaultTheme = require("tailwindcss/defaultTheme");

const theme = {
  ...defaultTheme,
  extend: {
    colors: {
      primary: "#00C4FF"
    },
    fill: theme => ({
      primary: theme("colors.pimary"),
      transparent: "transparent"
    }),
    stroke: theme => ({
      primary: theme("colors.pimary"),
      transparent: "transparent"
    }),
    fontFamily: {
      display: [
        'Nunito',
        'sans-serif'
      ]
    }
  }
};

module.exports = {
  theme,
  variants: ["responsive", "hover", "focus", "disabled"],
  plugins: []
};
